﻿using System;

namespace DiplomskiRad.View
{
    class MentorView
    {
        public Guid Id { get; set; }
        public String PunoIme { get; set; }
    }
}
