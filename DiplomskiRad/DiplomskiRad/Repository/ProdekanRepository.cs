﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace DiplomskiRad.Repository
{
    class ProdekanRepository
    {
        ConnectionStringBuilder builder = new ConnectionStringBuilder();
        NalogRepository nalogRepository = new NalogRepository();

        public List<Entiteti.Prodekan> VratiSveProdekane()
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<Entiteti.Prodekan> prodekani = new List<Entiteti.Prodekan>();

            using (var cmd = new NpgsqlCommand("select * from diplomski.prodekan", conn))
            using (var reader = cmd.ExecuteReader())
                while(reader.Read())
                {
                    Entiteti.Prodekan prodekan = new Entiteti.Prodekan();

                    prodekan.Id = (Guid)reader.GetValue(0);
                    prodekan.PunoIme = (String)reader.GetValue(1);
                    prodekan.NalogId = (Guid)reader.GetValue(2);
                    prodekan.Aktivan = ((bool)reader.GetValue(3) ? "da" : "ne");

                    prodekani.Add(prodekan);
                }

            conn.Close();
            return prodekani;
        }

        private bool DeaktivirajProdekanaAkoPostoji(Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            bool postoji = false;
            Guid nalog = new Guid();

            using (var cmd = new NpgsqlCommand("select nalog_id from diplomski.prodekan " +
                            "where aktivan=true", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    nalog = (Guid)reader.GetValue(0);
                    postoji = true;
                    conn.Close();
                    if (nalog == nalogId)
                        return true;
                }
                else
                {
                    conn.Close();
                    return true;
                }

            if (postoji)
            {
                nalogRepository.PromeniUlogu(nalog, "mentor");

                conn.Open();

                using (var cmd = new NpgsqlCommand("update diplomski.prodekan set aktivan=false " +
                    "where nalog_id='" + nalog + "'", conn))
                    try
                    {
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        return true;
                    }
                    catch (Exception exc)
                    {
                        conn.Close();
                        return false;
                    }
            }
            return true;
        }

        private int PostaviOpetAkoJeVecBioProdekan(Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            bool bio = false;
            conn.Open();

            using (var cmd = new NpgsqlCommand("select id from diplomski.prodekan " +
                            "where nalog_id='" + nalogId + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    conn.Close();
                    bio = true;
                }
                else
                {
                    conn.Close();
                }
            conn.Close();

            if (bio)
            {
                nalogRepository.PromeniUlogu(nalogId, "mentor-prodekan");

                conn.Open();

                using (var cmd = new NpgsqlCommand("update diplomski.prodekan set aktivan=true " +
                                "where nalog_id = '" + nalogId + "'", conn))
                    try
                    {
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        return 1;
                    }
                    catch (Exception exc)
                    {
                        conn.Close();
                        return 2;
                    }
            }
            return 3;
        }

        public bool KreirajProdekana(String punoIme, Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());
            if (DeaktivirajProdekanaAkoPostoji(nalogId))
            {
                int ret = PostaviOpetAkoJeVecBioProdekan(nalogId);

                if (ret == 3)
                {
                    conn.Open();

                    using (var cmd = new NpgsqlCommand("insert into diplomski.prodekan (puno_ime, nalog_id, aktivan) " +
                        "values ('" + punoIme + "', '" + nalogId + "', true)", conn))
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception exc)
                        {
                            conn.Close();
                            return false;
                        }
                    conn.Close();
                    return true;
                }
                else if (ret == 1)
                    return true;
            }
            return false;
        }
    }
}
