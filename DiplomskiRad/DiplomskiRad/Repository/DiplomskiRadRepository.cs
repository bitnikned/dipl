﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;

namespace DiplomskiRad.Repository
{
    class DiplomskiRadRepository
    {
        ConnectionStringBuilder builder = new ConnectionStringBuilder();

        public View.DiplomskiView VratiDiplomskiRadStudenta(Guid studentId)
        {
            MentorRepository mentorRepository = new MentorRepository();

            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            View.DiplomskiView radView = new View.DiplomskiView();

            using (var cmd = new NpgsqlCommand("select * from diplomski.diplomski_rad " +
                "where student_id='" + studentId + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if(reader.Read())
                {
                    radView.Id = (Guid)reader.GetValue(0);
                    radView.ImeMentora = mentorRepository.VratiImeMentora((Guid)reader.GetValue(2));
                    radView.Stanje = (String)reader.GetValue(3);
                    radView.Katedra = (String)reader.GetValue(4);
                    if (!DBNull.Value.Equals(reader.GetValue(5)))
                        radView.DatumOdbrane = ((DateTime)reader.GetValue(5)).ToString("dd.MM.yyyy.");
                    if (!DBNull.Value.Equals(reader.GetValue(6)))
                        radView.ClanKomisije1 = (String)reader.GetValue(6);
                    if (!DBNull.Value.Equals(reader.GetValue(7)))
                        radView.ClanKomisije2 = (String)reader.GetValue(7);
                    radView.NazivDiplomskogRada = (String)reader.GetValue(8);
                    radView.NazivZadatka = (String)reader.GetValue(9);
                }

            conn.Close();

            return radView;
        }

        public String VratiStanjeDiplomskog(Guid id)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            String stanje;

            using (var cmd = new NpgsqlCommand("select stanje from diplomski.diplomski_rad " +
                            "where id = '" + id + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    stanje = (String)reader.GetValue(0);
                    conn.Close();
                    return stanje;
                }
                else
                {
                    conn.Close();
                    return null;
                }
        }

        public bool PrijaviDiplomskiRad(Guid studentId, Guid mentorId, String katedra, 
            String nazivRada, String nazivZadatka)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("insert into diplomski.diplomski_rad " +
                            "(student_id, mentor_id, stanje, katedra, naziv_rada, naziv_zadatka) " +
                            "values ('" + studentId + "', '" + mentorId + "', 'prijavljen', '"+ katedra + "'" +
                            ", '" + nazivRada + "', '"+ nazivZadatka + "')", conn))
                try
                {
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    return true;
                }
                catch(Exception exc)
                {
                    conn.Close();
                    return false;
                }
        }

        public List<View.DiplomskiView> VratiSveDiplomskeRadove()
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<Entiteti.DiplomskiRad> radovi = new List<Entiteti.DiplomskiRad>();

            using (var cmd = new NpgsqlCommand("select * from diplomski.diplomski_rad " +
                "order by katedra, stanje", conn))
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    Entiteti.DiplomskiRad rad = new Entiteti.DiplomskiRad();
                    rad.Id = (Guid)reader.GetValue(0);
                    rad.StudentId = (Guid)reader.GetValue(1);
                    rad.MentorId = (Guid)reader.GetValue(2);
                    rad.Stanje = (String)reader.GetValue(3);
                    rad.Katedra = (String)reader.GetValue(4);
                    if (!DBNull.Value.Equals(reader.GetValue(5)))
                        rad.DatumOdbrane = ((DateTime)reader.GetValue(5)).ToString("dd.MM.yyyy.");
                    if (!DBNull.Value.Equals(reader.GetValue(6)))
                        rad.ClanKomisije1 = (String)reader.GetValue(6);
                    if (!DBNull.Value.Equals(reader.GetValue(7)))
                        rad.ClanKomisije2 = (String)reader.GetValue(7);
                    rad.NazivDiplomskogRada = (String)reader.GetValue(8);
                    rad.NazivZadatka = (String)reader.GetValue(9);

                    radovi.Add(rad);
                }

            conn.Close();

            return this.PopuniView(radovi);
        }

        public bool PromeniStanjeDiplomskog(Guid id, String novoStanje)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("update diplomski.diplomski_rad set stanje=" +
                "'"+ novoStanje +"' where id='"+ id +"'", conn))
                try
                {
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    return true;
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
        }

        public bool PromeniStanjeDiplomskogStudenta(Guid studentId, String novoStanje)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("update diplomski.diplomski_rad set stanje=" +
                "'" + novoStanje + "' where student_id='" + studentId + "'", conn))
                try
                {
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    return true;
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
        }

        public List<View.DiplomskiView> VratiSveDiplomskeRadoveMentora(Guid mentorId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<Entiteti.DiplomskiRad> radovi = new List<Entiteti.DiplomskiRad>();

            using (var cmd = new NpgsqlCommand("select * from diplomski.diplomski_rad " +
                "where mentor_id='"+ mentorId + "' order by stanje", conn))
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    Entiteti.DiplomskiRad rad = new Entiteti.DiplomskiRad();
                    rad.Id = (Guid)reader.GetValue(0);
                    rad.StudentId = (Guid)reader.GetValue(1);
                    rad.MentorId = (Guid)reader.GetValue(2);
                    rad.Stanje = (String)reader.GetValue(3);
                    rad.Katedra = (String)reader.GetValue(4);
                    if (!DBNull.Value.Equals(reader.GetValue(5)))
                        rad.DatumOdbrane = ((DateTime)reader.GetValue(5)).ToString("dd.MM.yyyy.");
                    if (!DBNull.Value.Equals(reader.GetValue(6)))
                        rad.ClanKomisije1 = (String)reader.GetValue(6);
                    if (!DBNull.Value.Equals(reader.GetValue(7)))
                        rad.ClanKomisije2 = (String)reader.GetValue(7);
                    rad.NazivDiplomskogRada = (String)reader.GetValue(8);
                    rad.NazivZadatka = (String)reader.GetValue(9);

                    radovi.Add(rad);
                }

            conn.Close();

            return this.PopuniView(radovi);
        }

        public bool DodavanjeClanovaKomisije(Guid id, List<String> imena)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("update diplomski.diplomski_rad set " +
                    "clan_komisije_1='" + imena[0] + "', clan_komisije_2='" + imena[1] + "' " +
                    "where id='"+ id +"'", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
        }

        public List<View.DiplomskiView> VratiSveDiplomskeRadoveSefaKatedre(String katedra)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<Entiteti.DiplomskiRad> radovi = new List<Entiteti.DiplomskiRad>();

            using (var cmd = new NpgsqlCommand("select * from diplomski.diplomski_rad " +
                "where katedra='" + katedra + "' order by stanje", conn))
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    Entiteti.DiplomskiRad rad = new Entiteti.DiplomskiRad();
                    rad.Id = (Guid)reader.GetValue(0);
                    rad.StudentId = (Guid)reader.GetValue(1);
                    rad.MentorId = (Guid)reader.GetValue(2);
                    rad.Stanje = (String)reader.GetValue(3);
                    rad.Katedra = (String)reader.GetValue(4);
                    if (!DBNull.Value.Equals(reader.GetValue(5)))
                        rad.DatumOdbrane = ((DateTime)reader.GetValue(5)).ToString("dd.MM.yyyy.");
                    if (!DBNull.Value.Equals(reader.GetValue(6)))
                        rad.ClanKomisije1 = (String)reader.GetValue(6);
                    if (!DBNull.Value.Equals(reader.GetValue(7)))
                        rad.ClanKomisije2 = (String)reader.GetValue(7);
                    rad.NazivDiplomskogRada = (String)reader.GetValue(8);
                    rad.NazivZadatka = (String)reader.GetValue(9);

                    radovi.Add(rad);
                }

            conn.Close();

            return this.PopuniView(radovi);
        }

        public bool DodavanjeDatumaOdbrane(Guid id, String datum)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("update diplomski.diplomski_rad " +
                                "set datum_odbrane='" + datum + "' where id='"+ id +"'", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
            conn.Close();
            return true;
        }

        private List<View.DiplomskiView> PopuniView(List<Entiteti.DiplomskiRad> radovi)
        {
            StudentRepository studentRepository = new StudentRepository();
            MentorRepository mentorRepository = new MentorRepository();

            List<View.DiplomskiView> radoviView = new List<View.DiplomskiView>();

            foreach (Entiteti.DiplomskiRad rad in radovi)
            {
                View.StudentView studentiView = studentRepository.VratiStudentaView(rad.StudentId);
                View.DiplomskiView radView = new View.DiplomskiView();

                radView.Id = rad.Id;
                radView.ImeStudenta = studentiView.PunoIme;
                radView.BrojIndeksaStudenta = studentiView.BrojIndeksa;
                radView.Stanje = rad.Stanje;
                radView.Katedra = rad.Katedra;
                radView.DatumOdbrane = rad.DatumOdbrane;
                radView.ClanKomisije1 = rad.ClanKomisije1;
                radView.ClanKomisije2 = rad.ClanKomisije2;
                radView.DatumOdbrane = rad.DatumOdbrane;
                radView.MentorId = rad.MentorId;
                radView.StudentId = rad.StudentId;
                radView.ImeMentora = mentorRepository.VratiImeMentora(rad.MentorId);
                radView.NazivDiplomskogRada = rad.NazivDiplomskogRada;
                radView.NazivZadatka = rad.NazivZadatka;

                radoviView.Add(radView);
            }

            return radoviView;
        }

        public bool ObrisiRad(Guid id)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("delete from diplomski.dokument where diplomski_id='" + id + "'", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }

            using (var cmd = new NpgsqlCommand("delete from diplomski.diplomski_rad where id='" + id + "'", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
        }

        public bool Upload(Guid diplomskiId, Guid studentId, byte[] readText, string nazivDokumenta)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("insert into diplomski.dokument " +
                "(dokument, datum, naziv, student_id, diplomski_id) " +
                "values (@p, '"+ DateTime.Now.ToString("yyyy-MM-dd") + "', '"+ nazivDokumenta +"', " +
                "'"+ studentId +"', '"+ diplomskiId +"')", conn))
                try
                {
                    NpgsqlParameter param = cmd.CreateParameter();
                    param.ParameterName = "@p";
                    param.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Bytea;
                    param.Value = readText;
                    cmd.Parameters.Add(param);

                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
            conn.Close();
            return true;
        }

        public List<DownloadRes> DownloadSve(Guid diplomskiId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<DownloadRes> dokumenti = new List<DownloadRes>();

            using (var cmd = new NpgsqlCommand("select id, dokument, datum, naziv from diplomski.dokument " +
                "where diplomski_id='"+ diplomskiId + "' order by datum desc", conn))
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    DownloadRes downloadRes = new DownloadRes();

                    downloadRes.Id = (Guid)reader.GetValue(0);
                    downloadRes.Dokument = (byte[])reader.GetValue(1);
                    downloadRes.Datum = ((DateTime)reader.GetValue(2)).ToString("dd.MM.yyyy.");
                    downloadRes.Naziv = (String)reader.GetValue(3);

                    dokumenti.Add(downloadRes);
                }

            conn.Close();
            return dokumenti;
        }

        public DownloadRes Download(Guid id)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("select dokument, datum, naziv from diplomski.dokument " +
                "where id='" + id + "'", conn))
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    DownloadRes downloadRes = new DownloadRes();
                    downloadRes.Dokument = (byte[])reader.GetValue(0);
                    downloadRes.Datum = ((DateTime)reader.GetValue(1)).ToString("dd.MM.yyyy.");
                    downloadRes.Naziv = (String)reader.GetValue(2);

                    return downloadRes;
                }

            conn.Close();
            return null;
        }
    }
}
