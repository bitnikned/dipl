﻿using System;
using System.Windows.Forms;

namespace DiplomskiRad
{
    public partial class Form1 : Form
    {
        public Prijava prijavaForm;

        ConnectionStringBuilder builder = new ConnectionStringBuilder();
        Repository.MentorRepository mentorRepository = new Repository.MentorRepository();
        Repository.SefKatedreRepository sefKatedreRepository = new Repository.SefKatedreRepository();
        Repository.AdminRepository adminRepository = new Repository.AdminRepository();

        public Form1()
        {
            InitializeComponent();

            //studentControl1.Visible = false;
            sluzbaControl1.Visible = false;
            mentorControl1.Visible = false;
            //adminControl1.Visible = false;
        }

        public void Ucitaj(String uloga, Guid nalogId)
        {
            switch (uloga)
            {
                //case "student":
                //    {
                //        studentControl1.UcitajStudenta(nalogId);
                //        studentControl1.Visible = true;
                //    }
                //    break;
                case "studentska_sluzba_radnik":
                    {
                        sluzbaControl1.nalogId = nalogId;
                        sluzbaControl1.InicijalizujPrikaz();
                        sluzbaControl1.Visible = true;
                    }
                    break;
                case "mentor":
                    {
                        Guid idMentora = mentorRepository.VratiIdMentora(nalogId);
                        mentorControl1.InicijalizujPrikaz(idMentora);

                        mentorControl1.Visible = true;
                        mentorControl1.uloga = MentorControl.Uloga.Mentor;
                    }
                    break;
                case "mentor-sef_katedre":
                    {
                        Guid idMentora = mentorRepository.VratiIdMentora(nalogId);
                        mentorControl1.uloga = MentorControl.Uloga.MentorSefKatedre;
                        mentorControl1.katedra = sefKatedreRepository.VratiNazivKatedre(nalogId);
                        mentorControl1.InicijalizujPrikaz(idMentora);

                        mentorControl1.Visible = true;
                    }
                    break;
                case "mentor-prodekan":
                    {
                        Guid idMentora = mentorRepository.VratiIdMentora(nalogId);
                        mentorControl1.uloga = MentorControl.Uloga.MentorProdekan;
                        mentorControl1.InicijalizujPrikaz(idMentora);
                        mentorControl1.Visible = true;
                    }
                    break;
                //case "admin":
                //    {
                //        adminControl1.Visible = true;
                //    }
                //    break;
                default:
                    break;
            }
        }

        private void btnNazad_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            prijavaForm.Show();
        }
    }
}
