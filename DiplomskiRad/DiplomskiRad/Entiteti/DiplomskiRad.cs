﻿using System;

namespace DiplomskiRad.Entiteti
{
    class DiplomskiRad
    {
        public Guid Id { get; set; }
        public Guid StudentId { get; set; }
        public Guid MentorId { get; set; }
        public String Stanje { get; set; }
        public String Katedra { get; set; }
        public String ClanKomisije1 { get; set; }
        public String ClanKomisije2 { get; set; }
        public String DatumOdbrane { get; set; }
        public String NazivDiplomskogRada { get; set; }
        public String NazivZadatka { get; set; }
    }
}
