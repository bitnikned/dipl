﻿using System;

namespace DiplomskiRad.Entiteti
{
    class SefKatedre
    {
        public Guid Id { get; set; }
        public String PunoIme { get; set; }
        public String Katedra { get; set; }
        public String Aktivan { get; set; }
        public Guid NalogId { get; set; }
    }
}
