﻿using System;

namespace DiplomskiRad.Entiteti
{
    class Mentor
    {
        public Guid Id { get; set; }
        public String PunoIme { get; set; }
        public String Katedra { get; set; }
        public Guid NalogId { get; set; }
    }
}
