﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace DiplomskiRad
{
    class ConnectionStringBuilder
    {
        public String getConnectionString()
        {
            var connStringBuilder = new NpgsqlConnectionStringBuilder();
            connStringBuilder.Host = "localhost";
            connStringBuilder.Port = 26257;
            connStringBuilder.SslMode = SslMode.Disable;
            connStringBuilder.Username = "user1";
            connStringBuilder.Database = "diplomski";

            return connStringBuilder.ConnectionString;
        }
    }
}
